package com.kfwebstandard.filterlogin.beans;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;

/**
 * Simple navigation bean
 *
 * @author itcuties http://www.itcuties.com/j2ee/jsf-2-login-filter-example/
 *
 * Modified to use faces-config.xml navigation by Ken Fogel
 *
 */
@Named
@RequestScoped
public class NavigationBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(NavigationBean.class.getName());

    /**
     * Redirect to login page.
     *
     * @return Login page navigation string.
     */
    public String redirectToLogin() {
        LOG.info("redirectToLogin");
        return "redirectToLogin";
    }

    /**
     * Go to login page.
     *
     * @return Login page navigation string.
     */
    public String toLogin() {
        LOG.info("toLogin");
        return "toLogin";
    }

    /**
     * Redirect to info page.
     *
     * @return Info page navigation string.
     */
    public String redirectToInfo() {
        LOG.info("redirectToInfo");
        return "redirectToInfo";
    }

    /**
     * Go to info page.
     *
     * @return Info page navigation string.
     */
    public String toInfo() {
        LOG.info("toInfo");
        return "toInfo";
    }

    /**
     * Redirect to welcome page.
     *
     * @return Welcome page navigation string.
     */
    public String redirectToWelcome() {
        LOG.info("redirectToWelcome");
        return "redirectToWelcome";
    }

    /**
     * Go to welcome page.
     *
     * @return Welcome page navigation string.
     */
    public String toWelcome() {
        LOG.info("toWelcome");
        return "toWelcome";
    }
}
